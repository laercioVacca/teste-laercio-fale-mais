/*
Origem |	Destino	| R$/minuto | minutos excedentes
11	   |    16	    | R$ 1,90   |   R$ 2.09 -  0.19
16	   |    11	    | R$ 2,90	|   R$ 3.19 -  0.29
11	   |	17	    | R$ 1,70	|   R$ 1,87 -  0.17
17	   |    11	    | R$ 2,70	|   R$ 2,97 -  0.27
11	   |    18	    | R$ 0,90	|   R$ 0,99 -  0.09
18	   |    11	    | R$ 1,90	|   R$ 2,09 -  0.19
*/ 

var argv = require('minimist')(process.argv.slice(2));

origem = argv.origem;
destino = argv.destino;
minutos = argv.minutos;


calcular(origem,destino,minutos)

function calcular(origem,Destino,minutos){
    var Origem = origem;
    var destino = Destino;
    var tempo;
    var flagValor;

    if(Origem === 11 && destino === 16)
    {
        flagValor = 1.90;
    }
    else if(Origem === 16 && destino === 11)
    {
        flagValor = 2.90;
    }
    else if(Origem === 11 && destino === 17)
    {
        flagValor = 1.70;
    }
     else if(Origem === 17 && destino === 11)
    {
        flagValor = 2.70;
    }
     else if(Origem === 11 && destino === 18)
    {
        flagValor = 0.90;
    }
    else if(Origem === 18 && destino === 11)
    {
        flagValor = 1.90;
    }
    else
    {
        flagValor = 'Nada encontrado!!';
    }

    console.log( planoFalemais30(minutos,flagValor) );
    console.log( planoFalemais60(minutos,flagValor) );
    console.log( planoFalemais120(minutos,flagValor) );
};


function planoFalemais30(minutos,flagValor){
    var SobraMinutos;
    var MinutosApagar;
    var MinutosUsados = minutos;
    var MinutosContratados = 30;
    var ValorMinutosPlano = flagValor/10;
    var ValorApagarSemPlano = minutos * flagValor;
    var arredondadoSP = parseFloat(ValorApagarSemPlano.toFixed(2));
    var ValorFinal = ValorMinutosPlano + flagValor;
    if(minutos < MinutosContratados)
    {
        MinutosApagar = 0;
        return 'Plano FaleMuito 30 R$ ' + MinutosApagar + ' | Valor sem Plano R$ '+arredondadoSP;
    }
    SobraMinutos = MinutosUsados - MinutosContratados ;
   
    MinutosApagar = SobraMinutos * ValorFinal;
    var arredondado = parseFloat(MinutosApagar.toFixed(2));
    return 'Plano FaleMuito 30 R$ ' + arredondado+ ' | Valor sem Plano R$ '+arredondadoSP;
}
function planoFalemais60(minutos,flagValor){
    var SobraMinutos;
    var MinutosApagar;
    var MinutosUsados = minutos;
    var MinutosContratados = 60;
    var ValorMinutosPlano = flagValor/10;
    var ValorApagarSemPlano = minutos * flagValor;
    var arredondadoSP = parseFloat(ValorApagarSemPlano.toFixed(2));
    var ValorFinal = ValorMinutosPlano + flagValor;
    if(minutos < MinutosContratados)
    {
        MinutosApagar = 0;
        return 'Plano FaleMuito 60 R$ ' + MinutosApagar + ' | Valor sem Plano R$ '+arredondadoSP;
    }
    SobraMinutos = MinutosUsados - MinutosContratados ;
   
    MinutosApagar = SobraMinutos * ValorFinal;
    var arredondado = parseFloat(MinutosApagar.toFixed(2));
    return 'Plano FaleMuito 60 R$ ' + arredondado + ' | Valor sem Plano R$ '+arredondadoSP;
}

function planoFalemais120(minutos,flagValor){
     var SobraMinutos;
    var MinutosApagar;
    var MinutosUsados = minutos;
    var MinutosContratados = 120;
    var ValorMinutosPlano = flagValor/10;
    var ValorApagarSemPlano = minutos * flagValor;
    var arredondadoSP = parseFloat(ValorApagarSemPlano.toFixed(2));
    var ValorFinal = ValorMinutosPlano + flagValor;
    if(minutos < MinutosContratados)
    {
        MinutosApagar = 0;
        return 'Plano FaleMuito 120 R$ ' + MinutosApagar + ' | Valor sem Plano R$ '+arredondadoSP;
    }
    SobraMinutos = MinutosUsados - MinutosContratados ;
   
    MinutosApagar = SobraMinutos * ValorFinal;
    var arredondado = parseFloat(MinutosApagar.toFixed(2));
    return 'Plano FaleMuito 120 R$ ' + arredondado+ ' | Valor sem Plano R$ '+arredondadoSP;
}


